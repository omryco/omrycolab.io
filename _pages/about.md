---
layout: page
# title: About
permalink: /about/
---

## Hi there! <span class="wave">👋</span>

My name is Omry Cohen. I'm a physics PhD student at the [Hebrew University of Jerusalem, Israel (HUJI)](https://twitter.com/HebrewU), under the supervision of [Dr. Zohar Ringel](http://old.phys.huji.ac.il/~zohar.ringel/).

My current research interest lies in computational aspects of quantum mechanics. Specifically, I'm interested in forming a theoretical understranding of quantum Monte Carlo (QMC) algorithms.

In the past, I studied [artificial neural networks (ANNs) from a field-theoretic point of view](https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.3.023034).

More generally, I'm thrilled about everything that has to do with thinking, learning and creating.

If you wish to contact me you can [DM me on twitter]({{site.accounts.twitter.url}}).

<center style="font-size:200%">

<a href="{{ site.accounts.twitter.url }}">{{ site.accounts.twitter.icon }}</a>
<a href="{{ site.accounts.linekdin.url }}">{{ site.accounts.linekdin.icon }}</a>
<a href="{{ site.accounts.googlescholar.url }}">{{ site.accounts.googlescholar.icon }}</a>
<a href="{{ site.accounts.orcid.url }}">{{ site.accounts.orcid.icon }}</a>
<a href="{{ site.accounts.gitlab.url }}">{{ site.accounts.gitlab.icon }}</a>
<a href="{{ site.accounts.github.url }}">{{ site.accounts.github.icon }}</a>

</center>
